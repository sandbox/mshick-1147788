<?php

/**
 * Theme function for custom data. 
 * Could potentially be overridden for complex presentations
 */
function theme_slate_arrange_subqueue_form_data($element) {
  return drupal_render($element);
}

/**
 * Slate theme to allow override for handling custom data in your head
 */
function theme_slate_head($data) {
  return false;
}

function theme_slate_arrange_subqueue_form($form) {
  $output = '';

  $subqueue = $form['#subqueue'];

  // get css to hide some of the help text if javascript is disabled
  drupal_add_css(drupal_get_path('module', 'nodequeue') .'/nodequeue.css');
  drupal_add_css(drupal_get_path('module', 'slate') .'/slate.css');

  // TODO: Would be nice to expose qid, sqid, reference as classes for more custom theming :).
  // TODO: Create unique ID to make multiple tabledrag forms on a page possible
  drupal_add_js(drupal_get_path('module', 'slate') .'/slate_dragdrop.js');

  // render custom data separately
  $output .= drupal_render($form['data']);

  // render subqueue active checkbox
  $output .= drupal_render($form['active']);

  // render form as table rows
  $rows = array();
  $counter = 1;
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['title'])) {
      $row = array();
  
      switch($form[$key]['date']['#active']) { 
        case 1:
          $row_active = 'active ';
          break;
        case 0:
          $row_active = 'inactive';
          break;
        case -1:
          $row_active = 'unset';
          break;
      }
      $row_active .= $form[$key]['position']['#active'] ? 'active-position' : null;
      
      $row[] = drupal_render($form[$key]['title']);
      $row[] = drupal_render($form[$key]['author']);
      $row[] = drupal_render($form[$key]['date']);
      $row[] = drupal_render($form[$key]['position']);    
      $row[] = drupal_render($form[$key]['edit']);
      $row[] = drupal_render($form[$key]['remove']);
  
      $rows[] = array(
        'data'  => $row,
        'class' => $row_active,
      );
    }
    
    $counter++;
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No nodes in this queue.'), 'colspan' => 7));
  }

  // render the main nodequeue table
  $header = array(t('Title'), t('Author'), t('Listing Time'), t('Position'), array('data' => t('Operations'), 'colspan' => 2));
  $output .= theme('table', $header, $rows, array('id' => 'nodequeue-slate', 'class' => 'nodequeue-slate nodequeue-dragdrop'));
  

  // render the autocomplete field for adding a node to the table
  $output .= '<div class="container-inline">';
  $output .= drupal_render($form['add']['nid']);
  $output .= drupal_render($form['add']['submit']);
  $output .= '</div>';

  // render the remaining form elements
  $output .= drupal_render($form);

  return $output;
}

/**
  * Theme function for the slate_insert element
  */
function theme_slate_insert($element) {
  $output = '';

  if (isset($element['#children'])) {
    $output = $element['#children'];
  }
  return theme('form_element', $element, $output);
}

/**
  * Theme function for the slate_insert element
  */
function theme_slate($element) {
  return '<div id="slate-items">' . $element['#children'] . '</div>';
}
