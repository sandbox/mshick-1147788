<?php

/**
  * If we don't have a subqueue already, get the active subqueue
  */
function slate_page($queue, $subqueue = false) {
  if(!$subqueue) {
    $subqueue = nodequeue_load_subqueue($queue->active_subqueue);
  }
  drupal_set_title(check_plain($subqueue->title));
  
  // Build breadcrumb based on queue and subqueue:
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l($queue->title, 'slate/page/'. $queue->qid);
  $breadcrumb[] = l($subqueue->title, 'slate/page/'. $queue->qid . '/' . $subqueue->sqid);
  drupal_set_breadcrumb($breadcrumb);
  $output = slate_view_nodes($subqueue->sqid, TRUE, TRUE);

  return $output ? $output : drupal_not_found();
}

/**
 * Callback to take user to admin the currently active slate
 */
function slate_admin_active($queue) {
  $qid = $queue->qid;
  $sqid = $queue->active_subqueue;
  drupal_goto("admin/content/nodequeue/{$qid}/view/{$sqid}");
}

/**
 * Callback to take user to admin the currently active slate
 */
function slate_add_subqueue_form(&$form_state, $queue) {
  // Adjust properties of the page so our subqueue is in the right
  // visual place.
  drupal_set_title(t("Add slate subqueue"));
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l($queue->title, "admin/content/nodequeue/$queue->qid");
  drupal_set_breadcrumb($breadcrumb);

  $form['datetime'] = array(
    '#title' => t('Date'),
    '#type' => 'date_popup',
    '#default_value' => FALSE,
    '#description' => t('Choose a day to create a new slate subqueue for.'),
    '#date_format' => 'Y-m-d',
    '#date_text_parts' => array('year', 'month', 'day'),
  );
  $form['qid'] = array(
    '#type' => 'value',
    '#value' => $queue->qid,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
    
  return $form;
}

function slate_add_subqueue_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $reference = strtotime($values['datetime']);
  $queue = array('qid' => $values['qid']);
  // Load subqueue to test for whether it exists already
  $subqueue = slate_load_subqueue_by_reference((object) $queue, $reference);
  if($subqueue) {
    $message = t('This slate subqueue already exists. You may !link instead.', array('!link' => l('edit it', 'admin/content/nodequeue/' . $values['qid'] . '/view/' . $subqueue->sqid)));
    form_set_error('datetime', $message);
    return;
  }
  
  form_set_value($form['datetime'], $reference, $form_state);
}

function slate_add_subqueue_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $queue = array('qid' => $values['qid']);
  $subqueue = slate_add_subqueue((object) $queue, $values['datetime']);
  if($subqueue = slate_add_subqueue((object) $queue, $values['datetime'])) {
    drupal_goto('admin/content/nodequeue/'. $queue['qid'] .'/view/'. $subqueue->sqid);
  }
}

/**
 * Helper function for autocompletion
 */
function slate_autocomplete($string = '') {
  // The user enters a comma-separated list of tags. We only autocomplete the last tag.
  $array = drupal_explode_tags($string);

  // Fetch last tag
  $last_string = trim(array_pop($array));
  $matches = array();
  if ($last_string != '') {
    $result = db_query_range(db_rewrite_sql("SELECT nq.qid, nq.title FROM {nodequeue_queue} nq WHERE nq.owner = 'slate' AND LOWER(nq.title) LIKE LOWER('%%%s%%')", 'nq', 'qid'), $last_string, 0, 10);

    $prefix = count($array) ? implode(', ', $array) .', ' : '';

    while ($queue = db_fetch_object($result)) {
      $n = $queue->title;
      // Commas and quotes in terms are special cases, so encode 'em.
      if (strpos($queue->title, ',') !== FALSE || strpos($queue->title, '"') !== FALSE) {
        $n = '"'. str_replace('"', '""', $queue->title) .'"';
      }
      $matches[$prefix . $n] = check_plain($queue->title);
    }
  }

  drupal_json($matches);
}

