Drupal.behaviors.nodequeueClear = function(context) {
  $('#edit-clear').click(function(){
    // mark nodes for removal
    $('.node-slate-remove').each(function(i){
      $(this).val(true);
    });

    // remove table rows...
    rows = $('table.nodequeue-dragdrop tbody tr:not(:hidden)').hide();

    nodequeuePrependEmptyMessage();
    nodequeueInsertChangedWarning();

    return false;
  });
};

// @todo fix this
Drupal.behaviors.nodequeueRemoveNode = function(context) {
  $('a.nodequeue-remove').css('display', 'block');
  $('a.nodequeue-remove').click(function() {
    a = $(this).attr('id');
    a = '#' + a.replace('nodequeue-remove-', 'edit-') + '-slate-remove';
    $(a).val(true);

    // hide the current row
    $(this).parent().parent().fadeOut('fast', function(){
      if ($('table.nodequeue-dragdrop tbody tr:not(:hidden)').size() == 0) {
        nodequeuePrependEmptyMessage();
      }
      else {
        nodequeueRestripeTable()
        nodequeueInsertChangedWarning();
      }
    });

    return false;
  });
}

Drupal.behaviors.nodequeueClearTitle = function(context) {
  $('#edit-add-nid').focus(function(){
    if (this.value == this.defaultValue) {
			this.value = '';
      $(this).css('color', '#000');
		}
  }).blur(function(){
    if (!this.value.length) {
      $(this).css('color', '#999');
			this.value = this.defaultValue;
		}
  });
}

/**
 * Restripe the nodequeue table after removing an element or changing the
 * order of the elements.
 */
function nodequeueRestripeTable() {
  $('table.nodequeue-dragdrop tbody tr:not(:hidden)')
  .filter(':odd')
    .removeClass('odd').addClass('even')
      .end()
  .filter(':even')
    .removeClass('even').addClass('odd')
      .end();

  $('tr:visible td.position').each(function(i){
    $(this).html(i + 1);
  });
}

/**
 * Add a row to the nodequeue table explaining that the queue is empty.
 */
function nodequeuePrependEmptyMessage() {
  $('.nodequeue-dragdrop tbody').prepend('<tr class="odd"><td colspan="6">No nodes in this queue.</td></tr>');
}

/**
 * Display a warning reminding the user to save the nodequeue.
 */
function nodequeueInsertChangedWarning() {
  if (Drupal.tableDrag['nodequeue-dragdrop'].changed == false) {
    $(Drupal.theme('tableDragChangedWarning')).insertAfter('.nodequeue-dragdrop').hide().fadeIn('slow');
    Drupal.tableDrag['nodequeue-dragdrop'].changed = true;
  }
}
